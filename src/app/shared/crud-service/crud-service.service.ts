import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class HttpService {
  private baseUrl : string = 'http://localhost:8080/rest/';
  constructor(private http: HttpClient) { }
  
  // Get all data from server -- GET Method
  getAll (url : string): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl+url)
      .pipe(
        tap(getAllDatas => this.log(`fetched data`)),
        catchError(this.handleError('getAllE', []))
      );
  }

  // Get all data from server -- GET Method
  getPost (url : string): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl+url)
      .pipe(
        tap(getAllDatas => this.log(`fetched data`)),
        catchError(this.handleError('getAllE', []))
      );
  }

  // Get data using ID from server -- GET Method
  getDataById(url : string, id: number): Observable<any[]> {
    const geturl = `${this.baseUrl}/${url}/?id=${id}`;
    return this.http.get<any[]>(geturl)
    .pipe(
      tap(getByid => this.log(`fetched data id=${id}`)),
      catchError(this.handleError(`getDataE id=${id}`,[]))
    );
  }

  // Search data using string -- GET Method
  searchData(url : string,term: string): Observable<any[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    const geturl = `${this.baseUrl}/${url}/?search=${term}`;
    return this.http.get<any[]>(geturl).pipe(
      tap(_ => this.log(`Data Not Found Key - "${term}"`)),
      catchError(this.handleError<any[]>('searchHeroes',[]))
    );
  }

  //Save data to the server {JSON Format} -- POST Method
  postData (url : string,postData : any[]): Observable<any[]> {
    return this.http.post<any[]>(this.baseUrl+url, postData, httpOptions).pipe(
      tap(responseData => this.log(`added record`)),
      catchError(this.handleError('postDataE',[]))
    );
  }

  //Update data to the server by its id {JSON Format} -- PUT Method
  updateData (url : string,postData : any[],id: number): Observable<any[]> {
    const geturl = `${this.baseUrl}/${url}/?id=${id}`;
    return this.http.put<any[]>(geturl, postData, httpOptions)
    .pipe(
      tap(responseUpdate => this.log(`updated hero id=${id}`)),
      catchError(this.handleError('updateDataE',[]))
    );
  }

  //Delete data from the server by its id   -- PUT Method
  deleteData (url : string,id: number): Observable<any[]> {
    const geturl = `${this.baseUrl}/${url}/${id}`;
    return this.http.delete<any[]>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted data id=${id}`)),
      catchError(this.handleError('deleteDataE',[]))
    );
  }

  //Error Handling
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      
      // Send error to the requested page.
      return of(error as T);

      // Let the app keep running by returning an empty result.
      //return of(result as T);
    };
  }

  /** Log a HTTPService message with the MessageService */
  private log(message: string) {
  // this.messageService.add('HeroService: ' + message);
  console.log('HTTPService: ' + message);
  }
}
