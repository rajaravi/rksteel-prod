import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'partner', header: 'Partner'},
          {field: 'order_id', header: 'Order ID'},
          {field: 'purchase_return_no', header: 'Purchase Return No'},
          {field: 'return_date', header: 'Return Date'},
          {field: 'status', header: 'Status'},
          {field: 'delivered_percent', header: 'Delivered Percent'},
          {field: 'debit_percent', header: 'Debit Percent'}
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/purchase/transactions/purchase-returns/edit/'+event.data.id])
  }

}
