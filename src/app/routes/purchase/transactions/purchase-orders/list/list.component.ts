import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'supplier', header: 'Supplier'},
          {field: 'po_number', header: 'PO Number'},
          {field: 'po_date', header: 'PO Date'},
          {field: 'final_total', header: 'Final Total'},
          {field: 'status', header: 'Status'},
          {field: 'received_perc', header: 'Received %'},
          {field: 'invoice_perc', header: 'Invoice %'},
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/purchase/transactions/purchase-orders/edit/'+event.data.id])
  }

}
