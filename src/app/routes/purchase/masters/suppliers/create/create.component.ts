import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, FormArray } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { HttpService } from '../../../../../shared/crud-service/crud-service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})


export class CreateComponent implements OnInit {

  supplierForm: FormGroup;

  public value: any = {};
  // CALENDAR PROPS
  public dt: Date = new Date();
  public minDate: Date = void 0;

  // ng2Select
  public items: Array<any> = [{ 'id': 'a', 'text': 'Amsterdam' }, { 'id': 'b', 'text': 'Antwerp' }, { 'id': 'c', 'text': 'Athens' },
  { 'id': 'd', 'text': 'Barcelona' }, { 'id': 'e', 'text': 'Berlin' }, { 'id': 'f', 'text': 'Birmingham' },
  { 'id': 'g', 'text': 'Bradford' }, { 'id': 'h', 'text': 'Bremen' }, { 'id': 'i', 'text': 'Brussels' },
  { 'id': 'j', 'text': 'Bucharest' }, { 'id': 'j', 'text': 'Budapest' }, { 'id': 'k', 'text': 'Cologne' }];

  constructor(public fb: FormBuilder, public http: HttpService) {
    // Model Driven validation
    this.supplierForm = fb.group({
      'supplier' : [null],
      'customer': [null],
      'name': [null, Validators.required],
      'legal_name' : [null, Validators.required],
      'competitor': [null],
      'address' : [null],
      'contactname' : [null],
      'contactnumber' : [null],
      'fax_no' : [null],
      'territory' : [null],
      'website' : [null],
      'remarks' : [null],
      'contact_name' : [null],
      'contact_title' : [null],
      'contact_email' : [null],
      'designation' : [null],
      'phone':[null],
      'mobile' : [null],
      'contact_notes' : [null],
      'contact_fax' : [null],
      'gst_registration_type' : [null],
      'gstn' : [null],
      'tin_no' : [null],
      'pan_no' : [null],
      'cst_no' : [null],
      'cin_no' : [null],
      'supplier_code' : [null],
      'purchase_pricelist' : [null],
      'credit_period' : [null],
      'account_payable' : [null],
      'general_type' : [null]
    });

    (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.supplierForm.controls) {
      this.supplierForm.controls[c].markAsTouched();
    }
    if (this.supplierForm.valid) {
      console.log('Valid!', value);
      this.http.postData('postdata.php', value).subscribe(data => {
        console.log(value);
      });
    }

  }

  ngOnInit() {
  }

}
