import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { HttpService } from '../../../../../shared/crud-service/crud-service.service';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})


export class EditComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};
    
  // ng2Select
  public items: Array<any> = [{'id':'a','text':'Amsterdam'}, {'id':'b','text':'Antwerp'}, {'id':'c','text':'Athens'}, 
  {'id':'d','text':'Barcelona'}, {'id':'e','text':'Berlin'}, {'id':'f','text':'Birmingham'}, 
  {'id':'g','text':'Bradford'}, {'id':'h','text':'Bremen'}, {'id':'i','text':'Brussels'}, 
  {'id':'j','text':'Bucharest'},{'id':'j','text': 'Budapest'},{'id':'k','text':'Cologne'}];

  constructor(public fb: FormBuilder, public http: HttpService) {
    // Model Driven validation
    this.valForm = fb.group({
      'supplier' : [null],
      'customer': [null],
      'name': [null, Validators.required],
      'legal_name' : [null, Validators.required],
      'competitor': [null],
      'address' : [null],
      'contactname' : [null],
      'contactnumber' : [null],
      'fax_no' : [null],
      'territory' : [null],
      'website' : [null],
      'remarks' : [null],
      'contact_name' : [null],
      'contact_title' : [null],
      'contact_email' : [null],
      'designation' : [null],
      'phone':[null],
      'mobile' : [null],
      'contact_notes' : [null],
      'contact_fax' : [null],
      'gst_registration_type' : [null],
      'gstn' : [null],
      'tin_no' : [null],
      'pan_no' : [null],
      'cst_no' : [null],
      'cin_no' : [null],
      'supplier_code' : [null],
      'purchase_pricelist' : [null],
      'credit_period' : [null],
      'account_payable' : [null],
      'general_type' : [null]
    });
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
        this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
        console.log('Valid!');
    }
    console.log(value);
  }

  ngOnInit() {
  }

}
