import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
//Form
import { SelectModule } from 'ng2-select';
import { TextMaskModule } from 'angular2-text-mask';
import { TagInputModule } from 'ngx-chips';
import { CustomFormsModule } from 'ng2-validation';
//Data Table
import { Ng2TableModule } from 'ng2-table/ng2-table';

import { SharedModule } from '../../../../shared/shared.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'list', component: ListComponent },
  { path: 'create', component: CreateComponent },
  { path: 'edit/:id', component: EditComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    SelectModule,
    TextMaskModule,
    TagInputModule,
    CustomFormsModule,
    Ng2TableModule,
  ],
  declarations: [CreateComponent, EditComponent, ListComponent],
  exports: [
      RouterModule
  ]
})
export class PurchasePlannerModule { }
