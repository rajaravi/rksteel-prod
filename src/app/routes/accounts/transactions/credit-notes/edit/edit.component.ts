import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};
  // CALENDAR PROPS
  public dt: Date = new Date();
  public minDate: Date = void 0;
    
  // ng2Select
  public items: Array<any> = [{'id':'a','text':'Amsterdam'}, {'id':'b','text':'Antwerp'}, {'id':'c','text':'Athens'}, 
  {'id':'d','text':'Barcelona'}, {'id':'e','text':'Berlin'}, {'id':'f','text':'Birmingham'}, 
  {'id':'g','text':'Bradford'}, {'id':'h','text':'Bremen'}, {'id':'i','text':'Brussels'}, 
  {'id':'j','text':'Bucharest'},{'id':'j','text': 'Budapest'},{'id':'k','text':'Cologne'}];
    
  constructor(fb: FormBuilder) {
    // Model Driven validation
    this.valForm = fb.group({
      'for_sales_return': [null],
      'sales_return_no': [null],
      'partner': [null],
      'date': [null],
      'sales_person': [null],
      'territory': [null],
      'currency': [null],
      'billing_address': [null],
      'customer_state': [null],
      'customer_gst_registration_type': [null],
     'customer_gstin': [null, Validators.required],
     'gst_credit_sale_details': [null],
     'e_way_bill_no': [null, Validators.required],
     'delivery_address': [null],
     'description': [null],
    });
    (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
        this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
        console.log('Valid!');
    }
    console.log(value);
  }
  ngOnInit() {
  }

}

