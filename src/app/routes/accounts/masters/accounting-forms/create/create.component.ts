import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';
import { Router } from '@angular/router'; 
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};
    
  constructor(fb: FormBuilder, private http: HttpService, private router: Router) {
    // Model Driven validation
    this.valForm = fb.group({
        'form_name': [null, Validators.required],
        'apply_sales': [null],
        'apply_purchase':[null],
        'is_active':[null]
    });
  }

  submitForm($ev, value: any) {
    alert('dd');
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
        this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
        console.log('Valid!');

        this.http.postData('/warehouse/store', value).subscribe(data =>{
          console.log(data);
          this.router.navigate(['/stock/master/hsn-codes'])
      });
    }
    console.log(value);
  }
  ngOnInit() {
  }

}
