import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

    public cars: Array<any>;
    public cols: any[];
    selectedCars1: Array<any>;
    constructor(public http: HttpService, private router: Router) { }

    ngOnInit() {
        this.http.getAll('datatable1.php').subscribe(data =>{
            this.cars = data;
        })
        
        this.cols = [
            {field: 'id', header: '#'},
            {field: 'hsnCode', header: 'HSN Code'},
            {field: 'type', header: 'Type'},
            {field: 'description', header: 'Description'}
        ];
    }

    onRowSelect(event) {
        console.log(event.data);
        this.router.navigate(['/share/accounts/masters/payment-modes/edit/'+event.data.id])
    }


}
