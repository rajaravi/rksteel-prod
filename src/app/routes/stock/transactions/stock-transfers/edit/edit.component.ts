import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, FormArray } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { HttpService } from '../../../../../shared/crud-service/crud-service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})


export class EditComponent implements OnInit {

  valForm: FormGroup;

  public value: any = {};
  public en: any = {};
  // CALENDAR PROPS
  public dt: Date = new Date();
  public minDate: Date = void 0;

  // ng2Select
  public items: Array<any> = [{ 'id': 'a', 'text': 'Amsterdam' }, { 'id': 'b', 'text': 'Antwerp' }, { 'id': 'c', 'text': 'Athens' },
  { 'id': 'd', 'text': 'Barcelona' }, { 'id': 'e', 'text': 'Berlin' }, { 'id': 'f', 'text': 'Birmingham' },
  { 'id': 'g', 'text': 'Bradford' }, { 'id': 'h', 'text': 'Bremen' }, { 'id': 'i', 'text': 'Brussels' },
  { 'id': 'j', 'text': 'Bucharest' }, { 'id': 'j', 'text': 'Budapest' }, { 'id': 'k', 'text': 'Cologne' }];

  constructor(public fb: FormBuilder, public http: HttpService) {
    // Model Driven validation
    this.valForm = fb.group({
      'transfer_date': [null],
      'src_warehouse': [null, Validators.required],
      'dest_warehouse': [null, Validators.required],
      'receipt_date': [null],
      'dispatch_date': [null],
      'remarks': [null, Validators.required]
    });

    (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
      this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
      console.log('Valid!', value);
      this.http.postData('postdata.php', value).subscribe(data => {
        console.log(value);
      });
    }

  }
  orderForm: FormGroup;
  //salesItem: any[] = [];

  ngOnInit() {
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      today: 'Today',
      clear: 'Clear'
    };
    this.orderForm = this.fb.group({
      salesItem: this.fb.array([
        this.initLink()
      ])
    });
  }

  initLink() {
    return this.fb.group({
      item_name: ['', Validators.required],
      quantity: [''],
      uom: ['', Validators.required],
      item_description: ['', Validators.required],
      display_group: [''],
      warehouse: [''],
      remarks: ['']

    });
  }
  addLink() {
    const control = <FormArray>this.orderForm.controls['salesItem'];
    control.push(this.initLink());
  }
  removeLink(i: number) {
    const control = <FormArray>this.orderForm.controls['salesItem'];
    control.removeAt(i);
  }

}



