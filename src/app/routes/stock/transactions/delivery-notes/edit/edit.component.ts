import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};
  public en: any = {};

  // ng2Select
  public items: Array<any> = [{ 'id': 'a', 'text': 'Amsterdam' }, { 'id': 'b', 'text': 'Antwerp' }, { 'id': 'c', 'text': 'Athens' },
  { 'id': 'd', 'text': 'Barcelona' }, { 'id': 'e', 'text': 'Berlin' }, { 'id': 'f', 'text': 'Birmingham' },
  { 'id': 'g', 'text': 'Bradford' }, { 'id': 'h', 'text': 'Bremen' }, { 'id': 'i', 'text': 'Brussels' },
  { 'id': 'j', 'text': 'Bucharest' }, { 'id': 'j', 'text': 'Budapest' }, { 'id': 'k', 'text': 'Cologne' }];

  constructor(fb: FormBuilder, public http: HttpService) {
    // Model Driven validation
    this.valForm = fb.group({
      'numbering_series': [null, Validators.required],
      'warehouse': [null],
      'customer': [null],
      'reason': [null, Validators.required],
      'dispatch_through': [null],
      'delivery_address': [null, Validators.required],
      'customer_reference': [null],
      'delivery_date': [null],
      'sales_person': [null],
      'territory': [null],
      'currency': [null],
      'vehicle_no': [null],
      'payment_terms': [null],
      'customer_state': [null],
      'remarks': [null],
      'vehicle_number': [null],
      'consignee': [null],
      'consignee_state': [null],
      'billing_address': [null],
      'phone': [null],
      'mobile': [null],
      'po_date': [null],
      'invoice_date': [null],
      'order_date': [null],
      'order_no': [null],
      'invoice_no': [null],
      'contact_person': [null],
      'email': [null],
      'item_name': [null],
      'quantity': [null],
      'item_description': [null],
      'uom': [null],
      'display_group': [null]
    });
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
      this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
      console.log('Valid!');
      this.http.postData('postdata.php', value).subscribe(data => {
        console.log(value);
      });
    }
  }

  ngOnInit() {
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      today: 'Today',
      clear: 'Clear'
    };
  }

}
