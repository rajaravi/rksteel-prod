import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

    public cars: Array<any>;
    public cols: any[];
    selectedCars1: Array<any>;
    constructor(public http: HttpService) { }

    ngOnInit() {
        /*this.http.getAll('datatable.php').subscribe(data =>{
            this.cars = data;
        })*/
        
        this.cols = [
            {field: 'id', header: '#'},
            {field: 'groupName', header: 'Name'},
            {field: 'groupCode', header: 'Group Code'},
            {field: 'active', header: 'Active'}
        ];
    }


}
