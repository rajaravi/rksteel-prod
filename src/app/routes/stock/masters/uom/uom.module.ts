import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
//Form
import { SelectModule } from 'ng2-select';
import { TextMaskModule } from 'angular2-text-mask';
import { TagInputModule } from 'ngx-chips';
import { CustomFormsModule } from 'ng2-validation';
//Data Table
import {TableModule} from 'primeng/table';
import {PanelModule} from 'primeng/panel';

import { SharedModule } from '../../../../shared/shared.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'list', component: ListComponent },
  { path: 'create', component: CreateComponent },
  { path: 'edit/:id', component: EditComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    SelectModule,
    TextMaskModule,
    TagInputModule,
    CustomFormsModule,
    TableModule,
    PanelModule
  ],
  declarations: [
    ListComponent, CreateComponent, EditComponent
  ],
  exports: [
      RouterModule
  ]
})

export class UomModule { }
