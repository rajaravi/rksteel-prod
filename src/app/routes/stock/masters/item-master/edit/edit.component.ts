import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};

  // ng2Select
  public items: Array<any> = [{ 'id': 'a', 'text': 'Amsterdam' }, { 'id': 'b', 'text': 'Antwerp' }, { 'id': 'c', 'text': 'Athens' },
  { 'id': 'd', 'text': 'Barcelona' }, { 'id': 'e', 'text': 'Berlin' }, { 'id': 'f', 'text': 'Birmingham' },
  { 'id': 'g', 'text': 'Bradford' }, { 'id': 'h', 'text': 'Bremen' }, { 'id': 'i', 'text': 'Brussels' },
  { 'id': 'j', 'text': 'Bucharest' }, { 'id': 'j', 'text': 'Budapest' }, { 'id': 'k', 'text': 'Cologne' }];

  constructor(fb: FormBuilder) {
    // Model Driven validation
    this.valForm = fb.group({
      'name': [null, Validators.required],
      'product_type': [null],
      'barcode': [null],
      'commodity_tariff': [null],
      'tariff_code': [null],
      'category': [null],
      'group': [null],
      'uom_group': [null, Validators.required],
      'stock_uom': [null, Validators.required],
      'equipment_item': [null],
      'hsn_sac_code': [null],
      'item_production': [null, Validators.required],
      'keep_stock': [null, Validators.required],
      'allow_backorders': [null, Validators.required],
      'sale_item': [null, Validators.required],
      'purchase_item': [null, Validators.required],
      'account': [null]
    });
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
      this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
      console.log('Valid!');
    }
    console.log(value);
  }
  ngOnInit() {
  }

}

