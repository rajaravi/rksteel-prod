import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

    public cars: Array<any>;
    public cols: any[];
    selectedCars1: Array<any>;
    constructor(public http: HttpService) { }

    ngOnInit() {
        this.http.getAll('datatable.php').subscribe(data =>{
            this.cars = data;
        })
        
        this.cols = [
            {field: 'id', header: '#'},
            {field: 'name', header: 'Name'},
            {field: 'itemGroup', header: 'Item Group'},
            {field: 'itemCategory', header: 'Item Category'},
            {field: 'salesItem', header: 'Sales Item'},
            {field: 'purchaseItem', header: 'Purchase Item'},
            {field: 'active', header: 'Active'}
        ];
    }


}
