import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {

  valForm: FormGroup;
  public value: any = {};
  public dt: Date = new Date();
  public minDate: Date = void 0;
    
  // ng2Select
  public items: Array<any> = [{'id':'a','text':'Amsterdam'}, {'id':'b','text':'Antwerp'}, {'id':'c','text':'Athens'}, 
  {'id':'d','text':'Barcelona'}, {'id':'e','text':'Berlin'}, {'id':'f','text':'Birmingham'}, 
  {'id':'g','text':'Bradford'}, {'id':'h','text':'Bremen'}, {'id':'i','text':'Brussels'}, 
  {'id':'j','text':'Bucharest'},{'id':'j','text': 'Budapest'},{'id':'k','text':'Cologne'}];
    
  constructor(fb: FormBuilder, public http: HttpService) {
    // Model Driven validation
    this.valForm = fb.group({
      'batch_name': [null, Validators.required],
      'item_name': [null, Validators.required],
      'expdate':[this.dt],
      'description': [null]
    });
    (this.minDate = new Date()).setDate(this.minDate.getDate() - 1000);    
  }

  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }
  public refreshValue(value: any): void {
    this.value = value;
  }
  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }
  public typed(value: any): void {
    console.log('New search input: ', value);
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
        this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
      console.log('Valid!' + value);
      this.http.postData('postdata.php',value).subscribe(data =>{
        console.log(data);
         
      });
    }
    
  }
  ngOnInit() {
  }

}

