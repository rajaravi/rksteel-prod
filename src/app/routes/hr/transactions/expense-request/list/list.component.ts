import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'request_no', header: 'Request No'},
          {field: 'request_date', header: 'Request Date'},
          {field: 'employee', header: 'Employee'},
          {field: 'customer', header: 'Customer'},
          {field: 'amount', header: 'Amount'},
          {field: 'amt_outstanding', header: 'Amount Outstanding'},
          {field: 'remarks', header: 'Remarks'}
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/hr/transactions/expense-request/edit/'+event.data.id])
  }

}
