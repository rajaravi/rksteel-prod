import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'employee', header: 'Employee'},
          {field: 'payroll_no', header: 'Payroll No'},
          {field: 'payroll_date', header: 'Payroll Date'},
          {field: 'payroll_start', header: 'Payroll Start'},
          {field: 'payroll_end', header: 'Payroll End'},
          {field: 'payable_pay_days', header: 'Payable Pay Days'},
          {field: 'status', header: 'Status'},
          {field: 'net_pay', header: 'Net Pay'},
          {field: 'outstanding_amount', header: 'Outstanding Amount'}
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/hr/transactions/payrolls/edit/'+event.data.id])
  }

}
