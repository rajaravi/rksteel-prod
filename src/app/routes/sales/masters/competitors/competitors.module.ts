import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

//Data Table
import {TableModule} from 'primeng/table';

import { SharedModule } from '../../../../shared/shared.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'create', component: CreateComponent },
  { path: 'edit/:id', component: EditComponent }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    TableModule
  ],
  declarations: [ ListComponent, CreateComponent, EditComponent ],
  exports: [ RouterModule ]
})
export class CompetitorsModule { }
