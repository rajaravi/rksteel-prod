import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'customer', header: 'Customer'},
          {field: 'quote_no', header: 'Quote No'},
          {field: 'quote_date', header: 'Quote Date'},
          {field: 'final_total', header: 'Final Total'},
          {field: 'status', header: 'Status'}
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/sales/transactions/quotations/edit/'+event.data.id])
  }

}
