import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'customer', header: 'Customer'},
          {field: 'order_id', header: 'Order ID'},
          {field: 'sales_return_no', header: 'Sales Return No'},
          {field: 'sales_return_date', header: 'Sales Return Date'},
          {field: 'status', header: 'Status'},
          {field: 'received_percent', header: 'Received Percent'},
          {field: 'credit_percent', header: 'Credit Percent'},
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/sales/transactions/sales-returns/edit/'+event.data.id])
  }

}
