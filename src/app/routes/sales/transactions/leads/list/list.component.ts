import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'lead_no', header: 'Lead No'},
          {field: 'subject', header: 'Subject'},
          {field: 'customer', header: 'Customer'},
          {field: 'sales_person', header: 'Sales Person'},
          {field: 'status', header: 'Status'},
          {field: 'potential_revenue', header: 'Potential Revenue'},
          {field: 'followup_date', header: 'Followup Date'},
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/sales/transactions/leads/edit/'+event.data.id])
  }

}
