import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'event_name', header: 'Event Name'},
          {field: 'location', header: 'Location'},
          {field: 'from_date', header: 'From Date'},
          {field: 'to_date', header: 'To Date'},
          {field: 'due_date', header: 'Due Date'},
          
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/sales/transactions/events/edit/'+event.data.id])
  }

}
