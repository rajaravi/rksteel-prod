import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'; 
import * as _ from 'lodash';

import { HttpService } from '../../../../../shared/crud-service/crud-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public cars: Array<any>;
  public cols: any[];
  selectedCars1: Array<any>;
  constructor(public http: HttpService, private router: Router) { }

  ngOnInit() {
      // this.http.getAll('datatable1.php').subscribe(data =>{
      //     this.cars = data;
      // })
      
      this.cols = [
          {field: 'id', header: '#'},
          {field: 'customer', header: 'Customer'},
          {field: 'order_no', header: 'Order No'},
          {field: 'order_date', header: 'Order Date'},
          {field: 'delivery_date', header: 'Delivery Date'},
          {field: 'status', header: 'Status'},
          {field: 'delivered', header: 'Delivered %'},
          {field: 'invoiced', header: 'Invoiced %'},
      ];
  }

  onRowSelect(event) {
      console.log(event.data);
      this.router.navigate(['/share/sales/transactions/sales-orders/edit/'+event.data.id])
  }

}
