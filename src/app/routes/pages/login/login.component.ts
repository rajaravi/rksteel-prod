import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { HttpService } from '../../../shared/crud-service/crud-service.service';
import { Router } from '@angular/router'; 
import { error } from 'selenium-webdriver';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;

    constructor(public settings: SettingsService, fb: FormBuilder, private router: Router, public http: HttpService) {

        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'password': [null, Validators.required]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            console.log('Valid!' + value);
            this.http.postData('postdata.php',value).subscribe(data =>{
                localStorage.setItem('token', 'ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SnBjM01pT2lKUGJteHBibVVnU2xkVUlFSjFhV3hrWlhJaUxDSnBZWFFpT2pFMU1UWXpPREV3TURJc0ltVjRjQ0k2TVRVME56a3hOekF3TWl3aVlYVmtJam9pZDNkM0xtVjRZVzF3YkdVdVkyOXRJaXdpYzNWaUlqb2lhbkp2WTJ0bGRFQmxlR0Z0Y0d4bExtTnZiU0lzSWtkcGRtVnVUbUZ0WlNJNklrcHZhRzV1ZVNJc0lsTjFjbTVoYldVaU9pSlNiMk5yWlhRaUxDSkZiV0ZwYkNJNkltcHliMk5yWlhSQVpYaGhiWEJzWlM1amIyMGlMQ0pTYjJ4bElqcGJJazFoYm1GblpYSWlMQ0pRY205cVpXTjBJRUZrYldsdWFYTjBjbUYwYjNJaVhYMC5RRlpBSk91NU9sQkxRZ2dqdlJYSUVRVDNMUzQ2Nmsyb1pjZlljUHdaUjVv');
                console.log(data);
                this.router.navigate(['/share/home'])
            });
        }
    }

    ngOnInit() {

    }

}
