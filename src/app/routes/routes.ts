import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from './pages/login/login.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';

export const routes = [

    {
        path: 'share',
        component: LayoutComponent,
        children: [
            // Stock Order routes
            { path: 'share', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            /* Stock routes */
                //Master
            { path: 'stock/masters/item-master', loadChildren: './stock/masters/item-master/item-master.module#ItemMasterModule' },
            { path: 'stock/masters/item-groups', loadChildren: './stock/masters/item-groups/item-groups.module#ItemGroupsModule' },
            { path: 'stock/masters/item-categories', loadChildren: './stock/masters/item-categories/item-categories.module#ItemCategoriesModule' },
            { path: 'stock/masters/uom', loadChildren: './stock/masters/uom/uom.module#UomModule' },
            { path: 'stock/masters/warehouse', loadChildren: './stock/masters/warehouse/warehouse.module#WarehouseModule' },
            { path: 'stock/masters/item-batches', loadChildren: './stock/masters/item-batches/item-batches.module#ItemBatchesModule' },
            { path: 'stock/masters/stk-io-rsn', loadChildren: './stock/masters/stk-io-rsn/stk-io-rsn.module#StkIoRsnModule' },
            { path: 'stock/masters/inspection-agents', loadChildren: './stock/masters/inspection-agents/inspection-agents.module#InspectionAgentsModule' },
            { path: 'stock/masters/item-sns', loadChildren: './stock/masters/item-sns/item-sns.module#ItemSnsModule' },
            { path: 'stock/masters/hsn-codes', loadChildren: './stock/masters/hsn-codes/hsn-codes.module#HsnCodesModule' },
                //Transactions
            { path: 'stock/transactions/delivery-notes', loadChildren: './stock/transactions/delivery-notes/delivery-notes.module#DeliveryNotesModule' },
            { path: 'stock/transactions/receipt-notes', loadChildren: './stock/transactions/receipt-notes/receipt-notes.module#ReceiptNotesModule' },
            { path: 'stock/transactions/stock-transfers', loadChildren: './stock/transactions/stock-transfers/stock-transfers.module#StockTransfersModule' },
            
            /* Purchase Order routes */
                // Master
            { path: 'purchase/masters/suppliers', loadChildren: './purchase/masters/suppliers/suppliers.module#SuppliersModule' },
                // Transactions
            { path: 'purchase/transactions/rfq', loadChildren: './purchase/transactions/rfq/rfq.module#RfqModule' },
            { path: 'purchase/transactions/supplier-quot', loadChildren: './purchase/transactions/supplier-quot/supplier-quot.module#SupplierQuotModule' },
            { path: 'purchase/transactions/purchase-orders', loadChildren: './purchase/transactions/purchase-orders/purchase-orders.module#PurchaseOrdersModule' },
            { path: 'purchase/transactions/purchase-returns', loadChildren: './purchase/transactions/purchase-returns/purchase-returns.module#PurchaseReturnsModule' },
               // Reports
            { path: 'purchase/reports/purchase-planner', loadChildren: './purchase/reports/purchase-planner/purchase-planner.module#PurchasePlannerModule' },
            { path: 'purchase/reports/purchase-register', loadChildren: './purchase/reports/purchase-register/purchase-register.module#PurchaseRegisterModule' },
            { path: 'purchase/reports/accounts-payable', loadChildren: './purchase/reports/accounts-payable/accounts-payable.module#AccountsPayableModule' },
            { path: 'purchase/reports/poir', loadChildren: './purchase/reports/poir/poir.module#PoirModule' },          

            /* Sales routes - Starts Here */
                // Master
            { path: 'sales/masters/customers', loadChildren: './sales/masters/customers/customers.module#CustomersModule' },
            { path: 'sales/masters/customer-group', loadChildren: './sales/masters/customer-group/customer-group.module#CustomerGroupModule' },
            { path: 'sales/masters/price-lists', loadChildren: './sales/masters/price-lists/price-lists.module#PriceListsModule' },
            { path: 'sales/masters/lead-sources', loadChildren: './sales/masters/lead-sources/lead-sources.module#LeadSourcesModule' },
            { path: 'sales/masters/territories', loadChildren: './sales/masters/territories/territories.module#TerritoriesModule' },
            { path: 'sales/masters/exclusive-partners', loadChildren: './sales/masters/exclusive-partners/exclusive-partners.module#ExclusivePartnersModule' },
            { path: 'sales/masters/competitors', loadChildren: './sales/masters/competitors/competitors.module#CompetitorsModule' },
            { path: 'sales/masters/document-library', loadChildren: './sales/masters/document-library/document-library.module#DocumentLibraryModule' },
            { path: 'sales/masters/activity-types', loadChildren: './sales/masters/activity-types/activity-types.module#ActivityTypesModule' },
            { path: 'sales/masters/lead-stages', loadChildren: './sales/masters/lead-stages/lead-stages.module#LeadStagesModule' },
            { path: 'sales/masters/sales-lost-reason', loadChildren: './sales/masters/sales-lost-reason/sales-lost-reason.module#SalesLostReasonModule' },
            { path: 'sales/masters/sales-targets', loadChildren: './sales/masters/sales-targets/sales-targets.module#SalesTargetsModule' },
            { path: 'sales/masters/industries', loadChildren: './sales/masters/industries/industries.module#IndustriesModule' },
            { path: 'sales/masters/contacts', loadChildren: './sales/masters/contacts/contacts.module#ContactsModule' },
            //     // Transactions
            { path: 'sales/transactions/leads', loadChildren: './sales/transactions/leads/leads.module#LeadsModule' },
            { path: 'sales/transactions/quotations', loadChildren: './sales/transactions/quotations/quotations.module#QuotationsModule' },
            { path: 'sales/transactions/competitive-quotes', loadChildren: './sales/transactions/competitive-quotes/competitive-quotes.module#CompetitiveQuotesModule' },
            { path: 'sales/transactions/sales-orders', loadChildren: './sales/transactions/sales-orders/sales-orders.module#SalesOrdersModule' },
            { path: 'sales/transactions/sales-returns', loadChildren: './sales/transactions/sales-returns/sales-returns.module#SalesReturnsModule' },
            { path: 'sales/transactions/complaints', loadChildren: './sales/transactions/complaints/complaints.module#ComplaintsModule' },
            { path: 'sales/transactions/events', loadChildren: './sales/transactions/events/events.module#EventsModule' },
            { path: 'sales/transactions/stock-reservations', loadChildren: './sales/transactions/stock-reservations/stock-reservations.module#StockReservationsModule' },
            //     // Reports            
            // { path: 'sales/reports/leads-report', loadChildren: './sales/reports/leads-report/leads-report.module#LeadsReportModule' },								
            // { path: 'sales/reports/customer-map', loadChildren: './sales/reports/customer-map/customer-map.module#CustomerMapModule' },								
            // { path: 'sales/reports/sales-activity-report',loadChildren: './sales/reports/sales-activity-report/sales-activity-report.module#SalesActivityReportModule' },								
            // { path: 'sales/reports/sales-register', loadChildren: './sales/reports/sales-register/sales-register.module#SalesRegisterModule' },								
            // { path: 'sales/reports/accounts-receivable', loadChildren: './sales/reports/accounts-receivable/accounts-receivable.module#AccountsReceivableModule' },								
            // { path: 'sales/reports/collection-status', loadChildren: './sales/reports/collection-status/collection-status.module#CollectionStatusModule' },								
            // { path: 'sales/reports/quote-items-report', loadChildren: './sales/reports/quote-items-report/quote-items-report.module#QuoteItemsReportModule' },								
            // { path: 'sales/reports/sales-order-items-report', loadChildren: './sales/reports/sales-order-items-report/sales-order-items-report.module#SalesOrderItemsReportModule'},								
            // { path: 'sales/reports/sales-performance-report', loadChildren: './sales/reports/sales-performance-report/sales-performance-report.module#SalesPerformanceReportModule'},								
            // { path: 'sales/reports/target-actuals', loadChildren: './sales/reports/target-actuals/target-actuals.module#TargetActualsModule' },								
            // { path: 'sales/reports/lead-items-report', loadChildren: './sales/reports/lead-items-report/lead-items-report.module#LeadItemsReportModule' },								
            // { path: 'sales/reports/sales-pipeline-report', loadChildren: './sales/reports/sales-pipeline-report/sales-pipeline-report.module#SalesPipelineReportModule' },								
            // { path: 'sales/reports/contacts-report', loadChildren: './sales/reports/contacts-report/contacts-report.module#ContactsReportModule' },								
            // { path: 'sales/reports/project-quote-items-report', loadChildren: './sales/reports/project-quote-items-report/project-quote-items-report.module#ProjectQuoteItemsReportModule' },
            // { path: 'sales/reports/activity-calendar', loadChildren: './sales/reports/activity-calendar/activity-calendar.module#ActivityCalendarModule' }

            /* Accounts routes */
                // Master
            { path: 'accounts/masters/accounting-forms', loadChildren: './accounts/masters/accounting-forms/accounting-forms.module#AccountingFormsModule' },
            { path: 'accounts/masters/accounts', loadChildren: './accounts/masters/accounts/accounts.module#AccountsModule' },
            { path: 'accounts/masters/additional-charges', loadChildren: './accounts/masters/additional-charges/additional-charges.module#AdditionalChargesModule' },
            { path: 'accounts/masters/companies', loadChildren: './accounts/masters/companies/companies.module#CompaniesModule' },
            { path: 'accounts/masters/currencies', loadChildren: './accounts/masters/currencies/currencies.module#CurrenciesModule' },
            { path: 'accounts/masters/currency-exchange-rates', loadChildren: './accounts/masters/currency-exchange-rates/currency-exchange-rates.module#CurrencyExchangeRatesModule' },
            { path: 'accounts/masters/financial-years', loadChildren: './accounts/masters/financial-years/financial-years.module#FinancialYearsModule' },
            { path: 'accounts/masters/landing-costs', loadChildren: './accounts/masters/landing-costs/landing-costs.module#LandingCostsModule' },
            { path: 'accounts/masters/payment-modes', loadChildren: './accounts/masters/payment-modes/payment-modes.module#PaymentModesModule' },
            { path: 'accounts/masters/tax-codes', loadChildren: './accounts/masters/tax-codes/tax-codes.module#TaxCodesModule' },
            { path: 'accounts/masters/tax-rates', loadChildren: './accounts/masters/tax-rates/tax-rates.module#TaxRatesModule' },
            { path: 'accounts/masters/tax-types', loadChildren: './accounts/masters/tax-types/tax-types.module#TaxTypesModule' },
                // Transactions
            { path: 'accounts/transactions/accounts-followup', loadChildren: './accounts/transactions/accounts-followup/accounts-followup.module#AccountsFollowupModule' },
            { path: 'accounts/transactions/credit-notes', loadChildren: './accounts/transactions/credit-notes/credit-notes.module#CreditNotesModule' },
            { path: 'accounts/transactions/debit-notes', loadChildren: './accounts/transactions/debit-notes/debit-notes.module#DebitNotesModule' },
            { path: 'accounts/transactions/journal-vouchers', loadChildren: './accounts/transactions/journal-vouchers/journal-vouchers.module#JournalVouchersModule' },
            { path: 'accounts/transactions/landing-cost-vouchers', loadChildren: './accounts/transactions/landing-cost-vouchers/landing-cost-vouchers.module#LandingCostVouchersModule' },
            { path: 'accounts/transactions/payment-vouchers', loadChildren: './accounts/transactions/payment-vouchers/payment-vouchers.module#PaymentVouchersModule' },
            { path: 'accounts/transactions/purchase-invoices', loadChildren: './accounts/transactions/purchase-invoices/purchase-invoices.module#PurchaseInvoicesModule' },
            { path: 'accounts/transactions/receipt-vouchers', loadChildren: './accounts/transactions/receipt-vouchers/receipt-vouchers.module#ReceiptVouchersModule' },
            { path: 'accounts/transactions/sales-invoices', loadChildren: './accounts/transactions/sales-invoices/sales-invoices.module#SalesInvoicesModule' },
			
			/* Sales routes - Ends Here */
            
            /* HR - Starts Here */
            { path: 'hr/transactions/leave-request', loadChildren: './hr/transactions/leave-request/leave-request.module#LeaveRequestModule' },
            { path: 'hr/transactions/expense-request', loadChildren: './hr/transactions/expense-request/expense-request.module#ExpenseRequestModule' },
            { path: 'hr/transactions/timesheet', loadChildren: './hr/transactions/timesheet/timesheet.module#TimesheetModule' },
            { path: 'hr/transactions/timesheet-approve', loadChildren: './hr/transactions/timesheet-approve/timesheet-approve.module#TimesheetApproveModule' },
            { path: 'hr/transactions/leave-credits', loadChildren: './hr/transactions/leave-credits/leave-credits.module#LeaveCreditsModule' },
            { path: 'hr/transactions/tax-exemptions', loadChildren: './hr/transactions/tax-exemptions/tax-exemptions.module#TaxExemptionsModule' },
            { path: 'hr/transactions/pay-structure', loadChildren: './hr/transactions/pay-structure/pay-structure.module#PayStructureModule' },
            { path: 'hr/transactions/payrolls', loadChildren: './hr/transactions/payrolls/payrolls.module#PayrollsModule' },
            
            { path: 'hr/masters/employee', loadChildren: './hr/masters/employee/employee.module#EmployeeModule' },
            { path: 'hr/masters/expense-categories', loadChildren: './hr/masters/expense-categories/expense-categories.module#ExpenseCategoriesModule' },
            { path: 'hr/masters/holiday', loadChildren: './hr/masters/holiday/holiday.module#HolidayModule' },
            { path: 'hr/masters/location', loadChildren: './hr/masters/location/location.module#LocationModule' },
            { path: 'hr/masters/departments', loadChildren: './hr/masters/departments/departments.module#DepartmentsModule' },
            { path: 'hr/masters/pay-levels', loadChildren: './hr/masters/pay-levels/pay-levels.module#PayLevelsModule' },
            { path: 'hr/masters/leave-types', loadChildren: './hr/masters/leave-types/leave-types.module#LeaveTypesModule' },
            { path: 'hr/masters/leave-entitlements', loadChildren: './hr/masters/leave-entitlements/leave-entitlements.module#LeaveEntitlementsModule' },
            { path: 'hr/masters/payroll-components', loadChildren: './hr/masters/payroll-components/payroll-components.module#PayrollComponentsModule' },
            { path: 'hr/masters/tax-exemption-categories', loadChildren: './hr/masters/tax-exemption-categories/tax-exemption-categories.module#TaxExemptionCategoriesModule' },
            
            /* HR - Ends Here */
        ]   
    },

    // Not lazy-loaded routes
    { path: '', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
    { path: '404', component: Error404Component },
    { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: '404' }

];
