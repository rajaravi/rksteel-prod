<?php
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE');
header('Access-Control-Max-Age: 1000');
//header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
$menu[0] = array(
    "text"=> 'Home',
    "link"=> '/share/home',
    "icon"=> 'icon-home'
);
$menu[1] = array("text"=> 'Sales', "heading"=> true);
$menu[2] = array(
    "text"=> 'Masters',
    "link"=> '/share/sales/masters',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Customers', "link"=> '/share/sales/masters/customers'),        
        1=>array("text"=> 'Customer Group', "link"=> '/share/sales/masters/customer-group'),
        2=>array("text"=> 'Price List', "link"=> '/share/sales/masters/price-lists'),
        3=>array("text"=> 'Lead Sources', "link"=> '/share/sales/masters/lead-sources'),
        4=>array("text"=> 'Territories', "link"=> '/share/sales/masters/territories'),
        5=>array("text"=> 'Exclusive Partners', "link"=> '/share/sales/masters/exclusive-partners'),
        6=>array("text"=> 'Competitors', "link"=> '/share/sales/masters/competitors'),
        7=>array("text"=> 'Document Library', "link"=> '/share/sales/masters/document-library'),
        8=>array("text"=> 'Activity Types', "link"=> '/share/sales/masters/activity-types'),
        9=>array("text"=> 'Lead Stages', "link"=> '/share/sales/masters/lead-stages'),
        10=>array("text"=> 'Sales Lost Reason', "link"=> '/share/sales/masters/sales-lost-reason'),
        11=>array("text"=> 'Sales Targets', "link"=> '/share/sales/masters/sales-targets'),
        12=>array("text"=> 'Industries', "link"=> '/share/sales/masters/industries'),
        13=>array("text"=> 'Contacts', "link"=> '/share/sales/masters/contacts'),
    )
);
$menu[3] = array(
    "text"=> 'Transactions',
    "link"=> '/share/sales/transactions',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Leads', "link"=> '/share/sales/transactions/leads'),        
        1=>array("text"=> 'Quotations', "link"=> '/share/sales/transactions/quotations'),
        2=>array("text"=> 'Competitive Quotes', "link"=> '/share/sales/transactions/competitive-quotes'),
        3=>array("text"=> 'Sales Orders', "link"=> '/share/sales/transactions/sales-orders'),
        4=>array("text"=> 'Sales Returns', "link"=> '/share/sales/transactions/sales-returns'),
        5=>array("text"=> 'Complaints', "link"=> '/share/sales/transactions/complaints'),
        6=>array("text"=> 'Events', "link"=> '/share/sales/transactions/events'),
        7=>array("text"=> 'Stock Reservations', "link"=> '/share/sales/transactions/stock-reservations'),        
    )
);

$menu[4] = array("text"=> 'HR', "heading"=> true);
$menu[5] = array(
    "text"=> 'Transactions',
    "link"=> '/share/hr/transactions',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Attendance', "link"=> '/share/hr/transactions/leave-request'),
        1=>array("text"=> 'Leave Requests', "link"=> '/share/hr/transactions/leave-request'),
        2=>array("text"=> 'Expense Requests', "link"=> '/share/hr/transactions/expense-request'),
        3=>array("text"=> 'Timesheets', "link"=> '/share/hr/transactions/timesheet'),
        4=>array("text"=> 'Timesheet Approval', "link"=> '/share/hr/transactions/timesheet-approve'),
        5=>array("text"=> 'Leave Credits', "link"=> '/share/hr/transactions/leave-credits'),
        6=>array("text"=> 'Tax Exemptions', "link"=> '/share/hr/transactions/tax-exemptions'),
        7=>array("text"=> 'Pay Structures', "link"=> '/share/hr/transactions/pay-structure'),
        8=>array("text"=> 'Payrolls', "link"=> '/share/hr/transactions/payrolls'),
    )
);

$menu[6] = array(
    "text"=> 'Masters',
    "link"=> '/share/hr/masters',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Employee', "link"=> '/share/hr/masters/employee'),
        1=>array("text"=> 'Expense Categories', "link"=> '/share/hr/masters/expense-categories'),
        2=>array("text"=> 'Holiday', "link"=> '/share/hr/masters/holiday'),
        3=>array("text"=> 'Location', "link"=> '/share/hr/masters/location'),
        4=>array("text"=> 'Departments', "link"=> '/share/hr/masters/departments'),
        5=>array("text"=> 'Pay Levels', "link"=> '/share/hr/masters/pay-levels'),
        6=>array("text"=> 'Leave Types', "link"=> '/share/hr/masters/leave-types'),
        7=>array("text"=> 'Leave Entitlements', "link"=> '/share/hr/masters/leave-entitlements'),
        8=>array("text"=> 'Payroll Components', "link"=> '/share/hr/masters/payroll-components'),
        9=>array("text"=> 'Tax Exemption Categories', "link"=> '/share/hr/masters/tax-exemption-categories')
    )
);

$menu[7] = array("text"=> 'Purchase Order', "heading"=> true);
$menu[8] = array(
    "text"=> 'Transactions',
    "link"=> '/share/purchase/transactions',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'RFQ', "link"=> '/share/purchase/transactions/rfq'),
        1=>array("text"=> 'Supplier Quotations', "link"=> '/share/purchase/transactions/supplier-quot'),
        2=>array("text"=> 'Purchase Orders', "link"=> '/share/purchase/transactions/purchase-orders'),
        3=>array("text"=> 'Purchase Returns', "link"=> '/share/purchase/transactions/purchase-returns')        
    )
);
$menu[9] = array(
    "text"=> 'Masters',
    "link"=> '/share/purchase/masters',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Suppliers', "link"=> '/share/purchase/masters/suppliers')
    )
);
$menu[10] = array("text"=> 'Stock', "heading"=> true);
$menu[11] = array(
	"text"=> 'Transactions',
    "link"=> '/share/dashboard',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
        0=>array("text"=> 'Delivery Notes',"link"=> '/share/dashboard/v1'),
        1=>array("text"=> 'Receipt Notes',"link"=> '/share/dashboard/v2'),
        2=>array("text"=> 'Stock Transfer',"link"=> '/share/dashboard/v3')
    )
);

$menu[12] = array(
	"text"=> 'Master',
    "link"=> '/share/dashboard',
    "icon"=> 'icon-speedometer',
    "submenu"=> array(
		0=>array("text"=> 'Item Master',"link"=> '/share/dashboard/v1'),
        1=>array("text"=> 'Item Group',"link"=> '/share/dashboard/v2'),
        2=>array("text"=> 'Item Categories',"link"=> '/share/dashboard/v3')
    )
);
echo json_encode($menu);
?>